<?php


namespace Soen\Session;


use Soen\Http\Message\Factory\CookieFactory;
use Soen\Http\Message\Response;
use Soen\Http\Message\ServerRequest;

class Session
{
    public $driver;

    /**
     * session名
     * @var string
     */
    public $name = 'session_id';

    /**
     * session_id长度
     * @var int
     */
    public $idLength = 26;

    /**
     * 生存时间
     * @var int
     */
    public $maxLifetime = 7200;

    /**
     * 过期时间
     * @var int
     */
    public $cookieExpires = 0;

    /**
     * 有效的服务器路径
     * @var string
     */
    public $cookiePath = '/';

    /**
     * 有效域名/子域名
     * @var string
     */
    public $cookieDomain = '';

    /**
     * 仅通过安全的 HTTPS 连接传给客户端
     * @var bool
     */
    public $cookieSecure = false;

    /**
     * 仅可通过 HTTP 协议访问
     * @var bool
     */
    public $cookieHttpOnly = false;

    /**
     * session_id
     * @var string
     */
    protected $id = '';

    /**
     * @var ServerRequest
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * Key前缀
     * @var string
     */
    public $keyPrefix = 'SESSION:';

    public function __construct()
    {
        $this->driver = \App::redis();
    }

    public function start(ServerRequest $request, Response $response){
        $this->request  = $request;
        $this->response = $response;
        $sessionId = $request->getAttribute($this->name);
        if(!$sessionId){
            $sessionId = $this->createId();
        }
        $this->id = $sessionId;
    }

    /**
     * 创建session_id
     * @return string
     */
    protected function createId()
    {
        do {
            $sessionId = $this->randomAlphanumeric($this->idLength);
        } while ($this->driver->exists($sessionId));
        return $sessionId;
    }

    /**
     * 赋值
     * @param string $name
     * @param $value
     * @return bool
     */
    public function set(string $name, $value)
    {
        $saveKey = $this->getSaveKey($this->getId());
        // 赋值
        $this->driver->hSet($saveKey, $name, serialize($value));
        // 更新生存时间
        $this->driver->expire($saveKey, $this->maxLifetime);
        // 设置/更新cookie
        $factory = new CookieFactory();
        $cookie  = $factory->createCookie($this->name, $this->id, time() + $this->maxLifetime);
        $cookie->withDomain($this->cookieDomain)
            ->withPath($this->cookiePath)
            ->withSecure($this->cookieSecure)
            ->withHttpOnly($this->cookieHttpOnly);
        $this->response->withCookie($cookie);
        return true;
    }

    /**
     * 取值
     * @param string $name
     * @param null $default
     * @return mixed
     */
    public function get(string $name = '', $default = null)
    {
        $saveKey = $this->getSaveKey($this->getId());
        // 更新生存时间
        $this->driver->expire($saveKey, $this->maxLifetime);
        // 设置/更新cookie
        $factory = new CookieFactory();
        $cookie  = $factory->createCookie($this->name, $this->id, time() + $this->maxLifetime);
        $cookie->withDomain($this->cookieDomain)
            ->withPath($this->cookiePath)
            ->withSecure($this->cookieSecure)
            ->withHttpOnly($this->cookieHttpOnly);
        $this->response->withCookie($cookie);
        // 返回值
        if(!$name){
            if ($default) {
                return $default;
            }
            return unserialize($this->driver->hGetAll($saveKey));
        }
        $value = $this->driver->hGet($saveKey, $name);
        if($value){
            return unserialize($value);
        }
        return $default;
    }

    /**
     * 取所有值
     * @return array
     */
    public function all()
    {
        return $this->driver->hGetAll($this->getId());
    }

    /**
     * 删除
     * @param string $name
     * @return bool
     */
    public function delete(string $name)
    {
        return $this->driver->hDel($this->getId(), $name);
    }

    /**
     * 清除session
     * @return bool
     */
    public function clear()
    {
        return $this->driver->clear($this->getId());
    }

    /**
     * 判断是否存在
     * @param string $name
     * @return bool
     */
    public function has(string $name)
    {
        return $this->driver->hExists($this->getId(), $name);
    }

    /**
     * 获取session_id
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $sessionId
     * @return string
     */
    public function getSaveKey(string $sessionId){
        return $this->keyPrefix . $sessionId;
    }

    /**
     * 获取随机字符
     * @param $length
     * @return string
     */
    protected function randomAlphanumeric($length)
    {
        $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        $last  = 61;
        $str   = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[mt_rand(0, $last)];
        }
        return $str;
    }
    
    
    
}